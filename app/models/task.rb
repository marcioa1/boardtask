class Task < ApplicationRecord
  belongs_to :history

  validates :description, :history_id, presence: true
end
