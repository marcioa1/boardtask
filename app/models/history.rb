class History < ApplicationRecord
  belongs_to :owner, class_name: 'Person'
  belongs_to :requester, class_name: 'Person'
  has_many :tasks

  enum allowed_statuses: [:pending, :started, :delivered, :accepted]

  validates :name, uniqueness: true
  validates :name, :requester_id, :status, presence: true
  validates :status, inclusion: { in: allowed_statuses.keys }
  validate :finished_at_cannot_be_previous_than_started_at, :points_in_range

  def self.valid_points
    [1,2,3,5,8,13]
  end

  def points_in_range
    if !History.valid_points.include? self.points
      errors.add(:points, "Somente são aceitos : 1,2,3,5,8,13")
    end
  end

  def finished_at_cannot_be_previous_than_started_at
    if started_at.present? && finished_at.present? && finished_at < started_at
      errors.add(:finished_at,  "A data de término nõa pode ser anterior à data de início")
    end
  end

  def next_status
    case status
    when "pending"
      "started"
    when "started"
      "delivered"
    when "delivered"
      if self.tasks.map(&:done).include? false
        "delivered"
      else
        "accepted"
      end
    else
      ""
    end
  end

  def previous_status
    case status
    when "started"
      "pending"
    when "delivered"
      "started"
    when "accepted"
      "delivered"
    else
      ""
    end
  end

end
