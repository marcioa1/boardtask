class Person < ApplicationRecord
  enum role: [:manager, :developer]

  validates :name, :email, presence: true
  validates :email, uniqueness: true
  validates :name, uniqueness: { scope: :role,
    message: "Já existe alguém com este nome com esta função" }
end
