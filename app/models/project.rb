class Project < ApplicationRecord
  belongs_to :manager,  class_name: 'Person'

  validates :name, :manager_id, presence: true
  validates :name, uniqueness: true

  validate :manager,  if: :is_manager?

  def is_manager?
    if !Person.find(manager_id).manager?
      errors.add(:manager_id, ": precisa ser gerente para adicionar e  ser responsável por um projeto.")
    end
  end
end
