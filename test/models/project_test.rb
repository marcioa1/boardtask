require 'test_helper'

class ProjectTest < ActiveSupport::TestCase
  test "project must have to have unique name" do
    p2 = Project.new(name: "TaskBoard", manager_id: people(:marcio).id)
    assert_not p2.valid?
  end

  test "project has to be assigned to a manager" do
    p2 = Project.new(name: "TaskBoard 2", manager_id: people(:antonio).id)
    assert_not p2.valid?
  end
  
end
