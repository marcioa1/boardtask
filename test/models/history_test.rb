require 'test_helper'

class HistoryTest < ActiveSupport::TestCase

  test ': A history must have a name' do
    his = History.new( owner: people(:marcio), requester: people(:antonio),
      status: :pending)
    assert_not his.valid?
  end

  test "a history can't have duplicate name" do
    his = History.new(name: "Create History Model", owner: people(:marcio),
      requester: people(:antonio), status: :pending)
    assert_not his.valid?
  end

  test "must have a status" do
    his = History.new(name: "Create History Model", owner: people(:marcio),
      requester: people(:antonio), status: nil)
    assert_not his.valid?
  end

  test "must have a valid status" do
    his = histories(:create_history)
    his.status = :another
    assert_not his.valid?
  end

 test "can't finished a history before it begins" do
   his = histories(:create_history)
   his.finished_at =  his.started_at - 1.second
   assert_not his.valid?
 end

 test "history can only accept valid points" do
  his = histories(:create_history)
  his.points = 6
  assert_not his.valid?
 end

  test "next status could be started if current status is pending" do
    his = histories(:create_history)
    assert his.next_status == "started"
  end

  test "next status could be delivered if current status is started" do
    his = histories(:create_history)
    his.status = "started"
    assert his.next_status == "delivered"
  end

  test "next status could not change if current status is delivered and one taks were not done yet" do
    his = histories(:create_history)
    his.status = "delivered"
    assert his.next_status == "delivered"
  end

  test "next status could be accepted if current status is delivered and all tasks were done" do
    his = histories(:all_tasks_done)
    his.status = "delivered"
    assert his.next_status == "accepted"
  end

  test "previous status could be delivered if current is accepted" do
    his = histories(:create_history)
    his.status = "accepted"
    assert his.previous_status == "delivered"
  end

  test "previous status could be started if current is delivered" do
    his = histories(:create_history)
    his.status = "delivered"
    assert his.previous_status == "started"
  end

  test "previous status could be pending if current is started" do
    his = histories(:create_history)
    his.status = "started"
    assert his.previous_status == "pending"
  end


end
