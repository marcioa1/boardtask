require 'test_helper'

class PersonTest < ActiveSupport::TestCase
  test "person would be a developer when he/she is created without defined role" do
    p = Person.new(name: "Joao", email: "antonio@ar1mobile.com.br")
    assert p.developer?
  end

  test "person must have to have unique email" do
    p2 = Person.new(name: "Outro Antonio", email: "antonio@g.com")
    assert_not p2.valid?
  end

  test "person cannot have the same name as a mate in the same role" do
    p2 = Person.new(name: "Antonio", email: "antonio2@ar1mobile.com.br", role: :developer)
    assert_not p2.valid?
  end

  test "person can have the same name as a mate in a different role" do
    p2 = Person.new(name: "Antonio", email: "antonio2@ar1mobile.com.br", role: :manager)
    assert p2.valid?
  end


end
