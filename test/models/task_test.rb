require 'test_helper'

class TaskTest < ActiveSupport::TestCase

  test "description must be present" do
    task = Task.new(history_id: histories(:create_history).id)
    assert_not task.valid?
  end

  test "history_id must be present" do
    task = Task.new(description: "absence of history")
    assert_not task.valid?
  end


end
