class CreateProjects < ActiveRecord::Migration[5.1]
  def change
    create_table :projects do |t|
      t.string :name
      t.integer :manager_id , index: true

      t.timestamps
    end
    add_foreign_key :projects, :people, column: :manager_id
  end
end
