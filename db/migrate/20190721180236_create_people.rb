class CreatePeople < ActiveRecord::Migration[5.1]
  def change
    create_table :people do |t|
      t.string :name
      t.string :email
      t.integer :role, default: 1

      t.timestamps

    end
    add_index :people, :email, unique: true
  end
end
