class CreateHistories < ActiveRecord::Migration[5.1]
  def change
    create_table :histories do |t|
      t.string :name
      t.integer :requester_id, index: true
      t.string :status
      t.integer :owner_id, index: true
      t.text :description
      t.datetime :started_at
      t.datetime :finished_at
      t.datetime :deadline
      t.integer :points

      t.timestamps
    end
    add_foreign_key :histories, :people, column: :requester_id
    add_foreign_key :histories, :people, column: :owner_id
  end
end
